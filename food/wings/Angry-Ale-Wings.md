### Angry Ale Wings Recipe

##### Angry Ale Sauce

1 bottle Ale
1 cup Brown sugar
2 Tbs Garlic, minced
1/2 cup Jalapeño, minced
1/2 cup Cayenne hot sauce
1/4 cup Lemon juice
3 Tbs Butter

##### Wings

2 lbs   Chicken Wings
1 Tbs  Ancho Chile Salt
1 Tbs  Smoked Paprika
1 tsp   Black pepper
2 Tbs  Garlic, minced

##### Recipe:

* Place wings into a mixing bowl, and season with spices.  Toss well and pour out onto a sheet tray that has been sprayed with pan release.

* Place wings in oven and precook to 155 degrees, allow to cool to room temp. (or do a day in advance).  Before serving return to oven or fry to an internal temp of 165 degrees.

* Place all ingredients (except for butter) into a sauce pan and bring to a boil...reduce to a summer and cook for 12-15 minutes...or until wings are cooked.

* Reduce sauce to a syrup, swirl in butter. 

* Place wings in a warm mixing bowl, dress with sauce and plate.

* Serve with bread and butter pickles and roasted garlic ranch dressing. 
