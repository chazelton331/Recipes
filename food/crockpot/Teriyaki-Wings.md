### Teriyaki Chicken Wings

##### Steps

* 1/2 cup granulated sugar
* 1/2 cup packed brown sugar
* 1 tablespoon garlic powder
* 1 tablespoon ground ginger
* 1 cup soy sauce
* 1/4 cup pineapple juice
* 1/4 cup olive oil (I used Jalapeño and it worked out pretty well)
* about 4 lb chicken wings (24)

##### Instructions

In very large bowl, mix all ingredients, toss gently to coat chicken.

Add chicken + mixture to slow cooker; cook on High heat setting 4 to 5 hours or until juice of chicken is clear when thickest part is cut to bone (165°F).

Broil for 7 minutes at 500°F to give them a nice char.